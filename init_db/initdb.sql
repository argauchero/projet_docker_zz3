CREATE TABLE calculs (
    id varchar(255) PRIMARY KEY NOT NULL,
    calcul varchar(255) NOT NULL,
    resultat float,
    etat varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
