import mysql.connector, pika, time, json

print(" [*] Sleeping for 20 seconds.")
time.sleep(20)

print(" [*] Connecting to server...")
connection = pika.BlockingConnection(pika.ConnectionParameters(host = "rabbitmq"))
channel = connection.channel()
channel.queue_declare(queue = "task_queue", durable = True)

print(" [*] Waiting for messages.")

def callback(ch, method, properties, body):
    print(" [x] Received %s" % body)
    cmd = json.loads(body.decode())
    op = cmd["op"]
    id = cmd["id"]
    try:
        n1 = float(cmd["n1"])
        n2 = float(cmd["n2"])
        if op == "+":
            res = n1 + n2
        elif op == "-":
            res = n1 - n2
        elif op == "x":
            res = n1 * n2
        elif op == "/":
            res = n1 / n2
        else:
            raise ValueError
    except (ValueError, ZeroDivisionError):
        res = None
        pass

    ch.basic_ack(delivery_tag = method.delivery_tag)

    mydb = mysql.connector.connect(
        host = "mysqldb",
        user = "root",
        password = "root",
        database = "calculatrice"
    )
    cursor = mydb.cursor()
    if res:
        cursor.execute("UPDATE calculs SET etat = 'Termine', resultat = " + str(res) + " WHERE id = '" + str(id) + "'")
    else:
        cursor.execute("UPDATE calculs SET etat = 'Erreur' WHERE id = '" + str(id) + "'")
    mydb.commit()

    cursor.close()
    mydb.close()

channel.basic_qos(prefetch_count = 1)
channel.basic_consume(queue = "task_queue", on_message_callback = callback)
channel.start_consuming()
