import mysql.connector, pika, json, time
from flask import Flask, request, redirect, url_for, render_template
from uuid import uuid4

app = Flask(__name__)

print(" [*] Waiting 20 seconds for database initialization...")
time.sleep(20)
print(" [*] Waiting for computation.")

@app.route("/api/computations", methods = ["GET", "POST"])
def computation():
    if request.method == "POST":
        n1 = request.form["n1"]
        n2 = request.form["n2"]
        op = request.form["op"]
        id = uuid4()
        body = json.dumps({
            "n1": n1,
            "n2": n2,
            "op": op,
            "id": str(id)
            }
        )

        mydb = mysql.connector.connect(
            host = "mysqldb",
            user = "root",
            password = "root",
            database = "calculatrice"
        )
        cursor = mydb.cursor()
        cursor.execute("INSERT INTO calculs (id, calcul, etat) VALUES ('" + str(id) + "','" + str(n1) + str(op) + str(n2) + "','En cours')")
        mydb.commit()

        cursor.close()
        mydb.close()

        connection = pika.BlockingConnection(pika.ConnectionParameters(host = "rabbitmq"))
        channel = connection.channel()
        channel.queue_declare(queue = "task_queue", durable = True)
        channel.basic_publish(
            exchange = "",
            routing_key = "task_queue",
            body = body,
            properties = pika.BasicProperties(
                delivery_mode = 2,
            ))
        connection.close()
        return redirect(url_for("get_result", id = id))
    else:
        return render_template("calculatrice.html")

@app.route("/api/computations/<uuid:id>", methods = ["GET"])
def get_result(id):
    mydb = mysql.connector.connect(
        host = "mysqldb",
        user = "root",
        password = "root",
        database = "calculatrice"
    )
    cursor = mydb.cursor()
    cursor.execute("SELECT * FROM calculs WHERE id = '" + str(id) + "'")

    row_headers = [x[0] for x in cursor.description]

    results = cursor.fetchall()
    json_data = []
    for result in results:
        json_data.append(dict(zip(row_headers,result)))

    cursor.close()
    mydb.close()
    return render_template('calcul.html', calculs = json_data)

@app.route("/api/operations", methods = ["GET"])
def get_results():
    mydb = mysql.connector.connect(
        host = "mysqldb",
        user = "root",
        password = "root",
        database = "calculatrice"
    )
    cursor = mydb.cursor()
    cursor.execute("SELECT * FROM calculs")

    row_headers = [x[0] for x in cursor.description]

    results = cursor.fetchall()
    json_data = []
    for result in results:
        json_data.append(dict(zip(row_headers,result)))

    cursor.close()
    mydb.close()
    return render_template('resultats.html', calculs = json_data)

if __name__ == "__main__":
    app.run(debug = True, host = "0.0.0.0")
