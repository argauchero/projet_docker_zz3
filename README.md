# Projet_docker_zz3

## Deployment instructions
docker-compose up -d

## Link to calculator
Calculatrice accessible ici : http://localhost:5000/api/computations

## Warning
Un délai de 20 secondes est présent entre le moment où les conteneurs sont lancés et celui où la calculatrice est accessible, le temps que la base de données soit créée.
